import { GetServerSidePropsContext, GetServerSidePropsResult } from 'next'

import { resolveSession } from './sessions'

/**
 * Check for the route is unauthenticated
 * @param context - Next.js server side props context
 * @param redirect - Redirect to the target URL
 * @returns redirect props
 */
export async function unauthenticatedRoute(
  context: GetServerSidePropsContext,
  redirect = '/home'
) {
  const session = await resolveSession(context)

  if (session) {
    return {
      redirect: {
        destination: redirect,
        permanent: false
      }
    }
  }

  return {
    props: {}
  }
}

/**
 * Check for the route is authenticated
 * @param context - Next.js server side props context
 * @param redirect - Redirect to the target URL
 * @returns redirect props
 */
export async function authenticatedRoute(
  context: GetServerSidePropsContext,
  redirect = '/login'
): Promise<GetServerSidePropsResult<{}>> {
  const session = await resolveSession(context)

  if (!session) {
    return {
      redirect: {
        destination: `${redirect}?redirect=${encodeURIComponent(
          context.resolvedUrl
        )}`,
        permanent: false
      }
    }
  }

  return {
    props: {}
  }
}

/**
 * Check for the route is gated to staff
 * @param context - Next.js server side props context
 * @param redirect - Redirect to the target URL
 * @returns redirect props
 */
export async function staffRoute(
  context: GetServerSidePropsContext,
  redirect = '/'
): Promise<GetServerSidePropsResult<{}>> {
  const session = await resolveSession(context)

  if (!session?.isStaff) {
    return {
      redirect: {
        destination: redirect,
        permanent: false
      }
    }
  }

  return {
    props: {}
  }
}
