datasource db {
  provider             = "mysql"
  url                  = env("DATABASE_URL")
  referentialIntegrity = "prisma"
}

generator client {
  provider        = "prisma-client-js"
  binaryTargets   = ["native", "debian-openssl-1.1.x"]
  previewFeatures = ["filterJson", "referentialIntegrity"]
}

generator giraphql {
  provider = "prisma-giraphql-types"
}

model User {
  id             String @id @default(uuid())
  username       String @unique @default(uuid())
  email          String @unique
  hashedPassword String

  // Flags
  spammy     Boolean @default(false)
  isVerified Boolean @default(false)
  isStaff    Boolean @default(false)
  inWaitlist Boolean @default(true)

  // Timestamps
  createdAt  DateTime  @default(now())
  updatedAt  DateTime  @updatedAt
  featuredAt DateTime?

  // Relations
  posts                  Post[]
  ownedProducts          Product[]      @relation("productOwner")
  subscribedProducts     Product[]
  ownedCommunities       Community[]    @relation("communityOwner")
  moderatedCommunities   Community[]    @relation("communityModerator")
  joinedCommunities      Community[]
  sessions               Session[]
  profile                Profile?
  tip                    Tip?
  invite                 Invite?
  integrations           Integration?
  likes                  Like[]
  badges                 Badge[]
  topics                 Topic[]
  polls                  PollAnswer[]
  logs                   Log[]
  reports                Report[]
  following              User[]         @relation("follows")
  followedBy             User[]         @relation("follows")
  receivedNotification   Notification[] @relation("notificationReceiver")
  dispatchedNotification Notification[] @relation("notificationDispatcher")

  // Attributes
  @@map("users")
}

model Session {
  id        String  @id @default(uuid())
  isStaff   Boolean @default(false)
  ipAddress String?
  userAgent String?

  // Timestamps
  createdAt DateTime @default(now())
  expiresAt DateTime @default(now())

  // Relations
  user   User   @relation(fields: [userId], references: [id], onDelete: Cascade)
  userId String

  // Attributes
  @@map("sessions")
}

model Profile {
  id        String  @id @default(uuid())
  name      String
  avatar    String
  nftSource String?
  cover     String
  coverBg   String
  bio       String?
  location  String?
  website   String?
  discord   String?
  github    String?
  twitter   String?

  // Relations
  user   User   @relation(fields: [userId], references: [id], onDelete: Cascade)
  userId String @unique

  // Attributes
  @@map("profiles")
}

model Invite {
  id        String  @id @default(uuid())
  code      String?
  usedTimes Int     @default(0)

  // Relations
  user   User   @relation(fields: [userId], references: [id], onDelete: Cascade)
  userId String @unique

  // Attributes
  @@map("invites")
}

model Tip {
  id           String  @id @default(uuid())
  cash         String?
  paypal       String?
  github       String?
  buymeacoffee String?
  bitcoin      String?
  ethereum     String?

  // Relations
  user   User   @relation(fields: [userId], references: [id], onDelete: Cascade)
  userId String @unique

  // Attributes
  @@map("tips")
}

model Post {
  id          String   @id @default(uuid())
  title       String?
  body        String   @db.Text
  done        Boolean  @default(false)
  type        PostType @default(POST)
  attachments Json?
  hidden      Boolean  @default(false)

  // Timestamps
  createdAt DateTime @default(now())
  updatedAt DateTime @updatedAt

  // Relations
  user         User           @relation(fields: [userId], references: [id], onDelete: Cascade)
  userId       String
  product      Product?       @relation(fields: [productId], references: [id], onDelete: Cascade)
  productId    String?
  community    Community?     @relation(fields: [communityId], references: [id], onDelete: Cascade)
  communityId  String?
  likes        Like[]
  topics       PostTopic[]
  poll         Poll?
  nft          NFT?
  parentId     String?
  parent       Post?          @relation("replies", fields: [parentId], references: [id], onDelete: SetNull)
  replies      Post[]         @relation("replies")
  notification Notification[] @relation("post")
  report       Report[]       @relation("report")

  // Attributes
  @@map("posts")
}

model NFT {
  id      String @id @default(uuid())
  tokenId String
  address String

  // Relations
  post   Post?   @relation(fields: [postId], references: [id], onDelete: Cascade)
  postId String? @unique

  // Attributes
  @@map("nfts")
}

model Poll {
  id String @id @default(uuid())

  // Relations
  post    Post?        @relation(fields: [postId], references: [id], onDelete: Cascade)
  postId  String?      @unique
  answers PollAnswer[]

  // Attributes
  @@map("poll")
}

model PollAnswer {
  id    String @id @default(uuid())
  index Int
  title String

  // Relations
  poll   Poll   @relation(fields: [pollId], references: [id], onDelete: Cascade)
  pollId String
  voters User[]

  // Timestamps
  createdAt DateTime @default(now())

  // Attributes
  @@map("poll_answers")
}

model PostTopic {
  id String @id @default(uuid())

  // Relations
  post    Post?   @relation(fields: [postId], references: [id], onDelete: Cascade)
  postId  String?
  topic   Topic?  @relation(fields: [topicId], references: [id], onDelete: Cascade)
  topicId String?

  // Attributes
  @@map("post_topics")
}

model Topic {
  id          String  @id @default(uuid())
  name        String  @unique
  image       String?
  description String?

  // Relations
  posts    PostTopic[]
  starrers User[]

  // Attributes
  @@map("topics")
}

model Like {
  id String @id @default(uuid())

  // Relations
  user         User           @relation(fields: [userId], references: [id], onDelete: Cascade)
  userId       String
  post         Post?          @relation(fields: [postId], references: [id], onDelete: Cascade)
  postId       String?
  notification Notification[] @relation("like")

  // Attributes
  @@unique([userId, postId], name: "likeIdentifier")
  @@map("likes")
}

model Product {
  id          String  @id @default(uuid())
  name        String
  slug        String  @unique
  description String?
  avatar      String?

  // Social
  website     String?
  producthunt String?
  discord     String?
  github      String?
  twitter     String?

  // Timestamps
  createdAt DateTime @default(now())
  updatedAt DateTime @updatedAt

  // Relations
  owner        User?          @relation("productOwner", fields: [ownerId], references: [id], onDelete: Cascade)
  ownerId      String?
  subscribers  User[]
  posts        Post[]
  notification Notification[] @relation("product")

  // Attributes
  @@map("products")
}

model Community {
  id          String  @id @default(uuid())
  name        String
  slug        String  @unique
  description String?
  avatar      String?

  // Timestamps
  createdAt DateTime @default(now())

  // Relations
  owner      User   @relation("communityOwner", fields: [ownerId], references: [id], onDelete: Cascade)
  ownerId    String
  moderators User[] @relation("communityModerator")
  members    User[]
  posts      Post[]
  rules      Rule[]

  // Attributes
  @@map("communities")
}

model Rule {
  id          String  @id @default(uuid())
  index       Int
  name        String
  description String?

  // Relations
  community   Community? @relation(fields: [communityId], references: [id])
  communityId String?

  // Attributes
  @@map("community_rules")
}

model Badge {
  id          String  @id @default(uuid())
  name        String
  image       String
  description String?

  // Relations
  users User[]

  // Attributes
  @@map("badges")
}

model Notification {
  id       String           @id @default(uuid())
  message  String?
  isRead   Boolean          @default(false)
  entityId String           @unique
  type     NotificationType

  // Timestamps
  createdAt DateTime @default(now())
  updatedAt DateTime @updatedAt

  // Relations
  receiver     User     @relation("notificationReceiver", fields: [receiverId], references: [id], onDelete: Cascade)
  receiverId   String
  dispatcher   User     @relation("notificationDispatcher", fields: [dispatcherId], references: [id], onDelete: Cascade)
  dispatcherId String
  like         Like?    @relation("like", fields: [likeId], references: [id], onDelete: Cascade)
  likeId       String?
  product      Product? @relation("product", fields: [productId], references: [id], onDelete: Cascade)
  productId    String?
  post         Post?    @relation("post", fields: [postId], references: [id], onDelete: Cascade)
  postId       String?

  // Attributes
  @@map("notifications")
}

model Integration {
  id                  String  @id @default(uuid())
  wakatimeAPIKey      String?
  spotifyRefreshToken String?
  githubId            String? @unique
  ethAddress          String? @unique
  ethNonce            String?

  // Relations
  user   User   @relation(fields: [userId], references: [id], onDelete: Cascade)
  userId String @unique

  // Attributes
  @@map("integrations")
}

model Log {
  id       String        @id @default(uuid())
  action   LogActionType
  entityId String?

  // Timestamps
  createdAt DateTime @default(now())

  // Relations
  user   User   @relation(fields: [userId], references: [id], onDelete: SetNull)
  userId String

  // Attributes
  @@map("logs")
}

model Report {
  id       String     @id @default(uuid())
  message  String     @db.Text
  resolved Boolean    @default(false)
  type     ReportType

  // Timestamps
  createdAt DateTime @default(now())

  // Relations
  user   User    @relation(fields: [userId], references: [id], onDelete: SetNull)
  userId String
  post   Post?   @relation("report", fields: [postId], references: [id], onDelete: Cascade)
  postId String?

  // Attributes
  @@map("reports")
}

// Types
enum PostType {
  POST
  TASK
  QUESTION
  POLL
  COMMIT
  NFT
  REPLY
}

enum NotificationType {
  // Post
  POST_LIKE
  POST_REPLY

  // User
  USER_MENTION
  USER_FOLLOW
  USER_INVITE_FOLLOW

  // Product
  PRODUCT_SUBSCRIBE
}

enum LogActionType {
  // Auth
  LOGIN
  LOGOUT

  // Settings
  SETTINGS_UPDATE
  PASSWORD_UPDATE
}

enum ReportType {
  POST
  USER
  PRODUCT
  COMMUNITY
}
